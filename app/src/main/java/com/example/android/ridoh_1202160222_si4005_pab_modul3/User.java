package com.example.android.ridoh_1202160222_si4005_pab_modul3;

public class User {
    private String nama;
    private String pekerjaan;
    private final int avatar;

    public User(String nama, String pekerjaan, int avatar) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.avatar = avatar;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public int getAvatar() {
        return avatar;
    }
}

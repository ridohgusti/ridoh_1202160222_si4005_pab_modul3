package com.example.android.ridoh_1202160222_si4005_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterDetailUser extends RecyclerView.Adapter<AdapterDetailUser.ViewHolder> {

    ArrayList<User> daftar_User;
    Context mContext;

    public AdapterDetailUser(ArrayList<User> daftarUser, Context mContext) {
        this.daftar_User = daftarUser;
        this.mContext = mContext;
    }

    @Override
    public AdapterDetailUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.data_user, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(AdapterDetailUser.ViewHolder viewHolder, int antrian) {
        User current_User = daftar_User.get(antrian);
        viewHolder.bindTo(current_User);
    }

    @Override
    public int getItemCount() {
        return daftar_User.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView nama, pekerjaan;
        ImageView foto;
        int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.idnama);
            pekerjaan = itemView.findViewById(R.id.idJabatan);
            foto = itemView.findViewById(R.id.ava);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            nama.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    foto.setImageResource(R.drawable.pria);
                    break;
                case 2 :
                default:
                    foto.setImageResource(R.drawable.perempuan);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),DetailUser.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("pekerjaan",pekerjaan.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}

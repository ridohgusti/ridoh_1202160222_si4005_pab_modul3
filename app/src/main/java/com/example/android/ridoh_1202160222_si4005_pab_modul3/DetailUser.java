package com.example.android.ridoh_1202160222_si4005_pab_modul3;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailUser extends AppCompatActivity {

    private TextView namaDetail, pekerjaanDetail;
    private ImageView fotoDetail;
    private int avatarCode;
    private String mNama,mPekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);


        namaDetail = findViewById(R.id.detail_Name);
        pekerjaanDetail = findViewById(R.id.detail_Pekerjaan);
        fotoDetail = findViewById(R.id.pic_gender);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");
        avatarCode = getIntent().getIntExtra("gender",2);

        namaDetail.setText(mNama);
        pekerjaanDetail.setText(mPekerjaan);
        switch (avatarCode){
            case 1 :
                fotoDetail.setImageResource(R.drawable.pria);
                break;
            case 2 :
            default:
                fotoDetail.setImageResource(R.drawable.perempuan);
                break;
        }
    }
}